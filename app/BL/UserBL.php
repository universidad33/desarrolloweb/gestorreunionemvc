<?php

namespace App\BL;

use App\AO\UserAO;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\Helpers\JwtAuthHelper;
use Carbon\Carbon;
use App\Http\Controllers\Generic\ResponseController;


class UserBL
{
    private static $response = [];
    private static $excepcion = [ 
        'msm' => 'Error al consultar en la Base de Datos', 
        'status' => 500
    ];

    public static function login($credentials) {

        try {

            $validateUser = self::validateUser($credentials);
            
            if($validateUser) {
                if($validateUser->delected == 0)
                {
                    $token = Auth::login($validateUser);
                    $infoToken = [
                        'ip' => request()->ip(),
                        'token' => $token,
                        'user_id' => $validateUser->id,
                    ];

                    JwtAuthHelper::saveToken($infoToken);

                    $data = [
                        'message' => 'Usuario validado',
                        'token' => $token
                    ];

                    self::$response['data'] = $data;

                } else {
                        $message = 'Usuario no está activo';
                }
            } else {
                $message = "Usuario no reistrado en el sistema";
            }

            if (isset($message)) {
                self::$response['message'] = $message;
            }

        } catch (\Throwable $th) {
            self::$response = array(
                'status' => '500',
                'message' => $th,
            );
        }
        return self::$response;
    }

    public static function validateUser($credentials)
    {
        try {
            $user = UserAO::validateUser($credentials['email']);

            if(isset($user) && Crypt::decryptString($user->password) == $credentials['password'])
            {
                self::$response = [
                    'data' => $user,
                    'status' => 200
                ];
            }
        } catch (\Throwable $th) {
            return null;
        }
        return $user;
    }

    public static function register($request) {
        $saved = false;
        try {
            $validateUser = UserAO::validateUser($request->email);
            if(!$validateUser) {
                $user = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Crypt::encryptString($request->password),
                    'role_id' => $request->role_id,
                ];
                $saved = UserAO::register($user);
                if($saved) {
                    self::$response = [
                        'data' => 'Usuario almacenado', 
                        'mensaje' => 'Usuario almacenado', 
                        'status' => 200
                    ];
                }
            } else {
                self::$response = [
                    'data' => 'Este usuario ya existe en el sistema', 
                    'mensaje' => 'Este usuario ya existe en el sistema', 
                    'status' => 420
                ];
            }
        } catch (\Throwable $th) {
            if ($th instanceof \PDOException) 
            {
                self::$response = [
                    'data' => 'Ha ocurrido un error en bd', 
                    'status' => 500
                ];
            } else {
                self::$response = [
                    'data' => 'Error inesperado', 
                    'status' => 500
                ];
            }
        }
        return self::$response;
    }

    public static function createMeet($request)
    {
        dd(JwtAuthHelper::getCurrentUser());
        $date = Carbon::now('UTC')->subHours(5)->toDateTimeString();
        $infoMeet = [
            'fecha' => $date,
            'hora' => $request->hora,
            'asunto' => $request->asunto,
            'description' => $request->description,
            'responsable' => JwtAuthHelper::getCurrentUser()->id,
            'created_at' => $date,
            'updated_at' => $date,
        ];
        
        return true;
    }

    public static function getAllUsers()
    {
        try {
            $objData = UserAO::getAllUsers();
            self::$response = ['data' => $objData, 'msn' => 'Exito', 'status' => 200];
        } catch (\Throwable $th) {
            dd($th);
            self::$response = self::$excepcion ;
        }
        return ResponseController::objectResponse(self::$response);
    }

}