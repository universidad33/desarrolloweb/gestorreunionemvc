<?php

namespace App\BL;

use App\AO\TokenAO;

class TokenBL
{
    private static $response = [];
    private static $excepcion = [ 
        'msm' => 'Error al consultar en la Base de Datos', 
        'status' => 500
    ];

    public static function setToken($infoToken)
    {
        return TokenAO::setToken($infoToken);
    }

    public static function checkToken($token)
    {
        return self::$response = TokenAO::validateToken($token);
    }

}