<?php

namespace App\BL;

use App\AO\MeetAO;
use App\Helpers\JwtAuthHelper;
use App\Http\Controllers\Generic\ResponseController;
use Carbon\Carbon;

class MeetBL
{
    private static $response = [];
    private static $excepcion = [ 
        'msm' => 'Error al consultar en la Base de Datos', 
        'status' => 500
    ];

    public static function createMeet($request)
    {
        try {
            $date = Carbon::now('UTC')->subHours(5)->toDateTimeString();
            $infoMeet = [
                'fecha' => $date,
                'hora' => $request->hora,
                'asunto' => $request->asunto,
                'description' => $request->description,
                'responsable' => JwtAuthHelper::getCurrentUser()->id,
                'created_at' => $date,
                'updated_at' => $date,
            ];
            MeetAO::createMeet($infoMeet);
            self::$response = [
                'data' => true, 
                'mensaje' => 'Reunión creada!', 
                'status' => 200
            ];
        } catch (\Throwable $th) {
            self::$response = self::$excepcion;
        }
        return self::$response;
    }

    public static function getAllMeet()
    {
        try {
            $roles = MeetAO::getAllMeet();
            self::$response = ['data' => $roles, 'msn' => 'Exito', 'status' => 200];
        } catch (\Throwable $th) {
            dd($th);
        }
        return ResponseController::objectResponse(self::$response);
    }

    public static function addCompromisos($request)
    {
        dd(JwtAuthHelper::getCurrentUser());
        $date = Carbon::now('UTC')->subHours(5)->toDateTimeString();
        $infoMeet = [
            'fecha' => $date,
            'hora' => $request->hora,
            'asunto' => $request->asunto,
            'description' => $request->description,
            'responsable' => JwtAuthHelper::getCurrentUser()->id,
            'created_at' => $date,
            'updated_at' => $date,
        ];
        MeetAO::createMeet($infoMeet);
    }

}