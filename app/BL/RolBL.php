<?php

namespace App\BL;

use App\AO\RolAO;
use App\Helpers\JwtAuthHelper;
use App\Http\Controllers\Generic\ResponseController;
use Carbon\Carbon;

class RolBL
{
    private static $response = [];
    private static $excepcion = [ 
        'msm' => 'Error al consultar en la Base de Datos', 
        'status' => 500
    ];

    public static function getAllRoles()
    {
        try {
            $roles = RolAO::getAllRoles();
            self::$response = ['data' => $roles, 'msn' => 'Exito', 'status' => 200];
        } catch (\Throwable $th) {
            dd($th);
        }
        return ResponseController::objectResponse(self::$response);
    }

}