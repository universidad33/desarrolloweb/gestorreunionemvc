<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class SesionToken extends Model
{

    protected $table = 'sesion_token';
    /*
    * The attributes that are mass assignable.
    *
    * @var array<int, string>
    */
    protected $fillable = [
        'id',
        'ip',
        'token',
        'user_id',
        'logged',
    ];

    public function user_id() {
        return $this->belongsTo(User::class, "user_id");
    }

}
