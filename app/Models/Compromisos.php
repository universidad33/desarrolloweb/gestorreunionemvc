<?php

namespace App\Models;

use App\Models\ActaReunion;
use Illuminate\Database\Eloquent\Model;

class Compromisos extends Model
{
    /*
    * The attributes that are mass assignable.
    *
    * @var array<int, string>
    */
    protected $fillable = [
        'id',
        'description',
        'acta_id',
    ];

    public function acta_id() {
        return $this->belongsTo(ActaReunion::class, "acta_id");
    }

}
