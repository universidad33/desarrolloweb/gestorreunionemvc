<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ActaReunion extends Model
{

    protected $table = 'acta_reunion';
    /*
    * The attributes that are mass assignable.
    *
    * @var array<int, string>
    */
    protected $fillable = [
        'id',
        'fecha',
        'hora',
        'asunto',
        'description',
        'responsable',
    ];

    public function user_id() {
        return $this->belongsTo(User::class, "user_id");
    }

}
