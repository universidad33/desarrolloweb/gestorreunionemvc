<?php

namespace App\AO;

use App\Models\SesionToken;

class TokenAO
{
    public static function setToken($infoToken)
    {
        return SesionToken::insert($infoToken);
    }

    public static function validateToken($token)
    {
        return SesionToken::where('ip', $token['ip'])
            ->where('token', $token['token'])
            ->count();
    } 

}