<?php

namespace App\AO;

use App\Models\User;

class UserAO
{
    public static function validateUser($email)
    {
        return User::where('email', $email)->first();
    }

    public static function register($user)
    {
        return User::insert($user);
    }

    public static function createMeet($user)
    {
        return User::insert($user);
    }

    public static function getAllUsers()
    {
        return User::where('delected', 0)->get();
    }
}