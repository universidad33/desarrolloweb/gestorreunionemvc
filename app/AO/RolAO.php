<?php

namespace App\AO;

use App\Models\Role;

class RolAO
{

    public static function getAllRoles()
    {
        return Role::all();
    }
}