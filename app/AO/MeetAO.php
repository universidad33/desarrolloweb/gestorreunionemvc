<?php

namespace App\AO;

use App\Models\ActaReunion;

class MeetAO
{

    public static function createMeet($info)
    {
        return ActaReunion::insert($info);
    }

    public static function getAllMeet()
    {
        return ActaReunion::all();
    }
}