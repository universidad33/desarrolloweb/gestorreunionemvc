<?php

namespace App\Helpers;

use App\AO\TokenAO;
use Tymon\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;


class JwtAuthHelper
{

    public static function saveToken($token) 
    {
        return TokenAO::setToken($token);
    }

    public static function checkToken($token)
    {
        return TokenAO::validateToken($token);
    }

    public static function getCurrentUser() {
        return FacadesJWTAuth::parseToken()->authenticate();
    }
}