<?php

namespace App\Http\Controllers;

use App\BL\MeetBL;
use Illuminate\Http\Request;

class MeetController extends Controller
{
    public function createMeet(Request $request)
    {
        return MeetBL::createMeet($request);
    }

    public function getAllMeet(Request $request)
    {
        return MeetBL::getAllMeet($request);
    }

    public function addCompromisos(Request $request)
    {
        return MeetBL::createMeet($request);
    }
}
