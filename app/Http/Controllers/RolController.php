<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BL\RolBL;

class RolController extends Controller
{
    public function roles()
    {
        $response = RolBL::getAllRoles();
        return response()->json($response, 200);  
    }
}
