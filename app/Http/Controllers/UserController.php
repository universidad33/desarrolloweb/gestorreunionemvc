<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BL\UserBL;

class UserController extends Controller
{
    public function list(Request $request)
    {
        $response = UserBL::getAllUsers();
        return response()->json($response, 200);
    }
    
}
