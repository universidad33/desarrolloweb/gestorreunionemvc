<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\BL\UserBL;
use App\Helpers\JwtAuthHelper;


class UserAuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        return UserBL::login($credentials);
    }

    public function register(Request $request)
    {
        return UserBL::register($request);
    }

    public function me(Request $request)
    {
        return JwtAuthHelper::getCurrentUser();
    }

}
