<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Helpers\JwtAuthHelper;

class JwtAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $response = '';
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $infoToken = [
                'ip' => $request->ip(),
                'token' => $request->bearerToken(),
                'user_id' => $user->id,
            ];
            $request['userInfo'] = $infoToken;
            $sessionValidated = JwtAuthHelper::checkToken($infoToken);
            if($sessionValidated)
            {
               $response = $next($request);
            }  else {
                $response = [
                    'codigo'  => '999', 
                    'message' => 'Sesion invalida. '
                ];
            } 
            
        } catch (\Throwable $th) {
            $response = [
                'codigo'  => '999', 
                'message' => 'Sesion invalida.' . $th
            ];
            $response = $next($request);

        }
        return [$response];
    }
}
