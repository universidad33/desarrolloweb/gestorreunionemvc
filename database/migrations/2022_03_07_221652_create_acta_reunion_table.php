<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActaReunionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acta_reunion', function (Blueprint $table) {
            $table->id();
            $table->time('fecha');
            $table->string('hora', 20);
            $table->string('asunto', 40);
            $table->longText('description', 20);
            $table->unsignedBigInteger('responsable');
            $table->foreign('responsable')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acta_reunion');
    }
}
