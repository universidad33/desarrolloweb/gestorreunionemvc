-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-03-2022 a las 04:31:26
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestor_reuniones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acta_reunion`
--

CREATE TABLE `acta_reunion` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha` time NOT NULL,
  `hora` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asunto` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsable` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `acta_reunion`
--

INSERT INTO `acta_reunion` (`id`, `fecha`, `hora`, `asunto`, `description`, `responsable`, `created_at`, `updated_at`) VALUES
(2, '18:48:00', '23', 'Reuniónd', 'wedwed', 1, '2022-03-05 23:48:13', '2022-03-05 23:48:14'),
(3, '20:38:46', 'wedwed', 'wedwed', 'wedweddew', 1, '2022-03-06 01:38:46', '2022-03-06 01:38:46'),
(4, '20:41:43', 'wedwed', 'wedwed', 'dwe', 1, '2022-03-06 01:41:43', '2022-03-06 01:41:43'),
(5, '20:45:21', 'wedwed', 'wedwedwed', 'dwewed', 1, '2022-03-06 01:45:21', '2022-03-06 01:45:21'),
(6, '20:53:37', 'eeee', 'eeee', 'eee', 1, '2022-03-06 01:53:37', '2022-03-06 01:53:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compromisos`
--

CREATE TABLE `compromisos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `acta_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_asistentes`
--

CREATE TABLE `lista_asistentes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2022_03_04_215839_create_role_table', 1),
(3, '2022_03_05_000000_create_users_table', 1),
(4, '2022_03_06_215912_create_sesion_token_table', 1),
(5, '2022_03_07_221652_create_acta_reunion_table', 1),
(6, '2022_03_08_221712_create_lista_asistentes_table', 1),
(7, '2022_03_09_221726_create_compromisos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `desciption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `desciption`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2022-03-05 19:40:57', '2022-03-05 19:40:58'),
(2, 'de', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion_token`
--

CREATE TABLE `sesion_token` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `logged` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sesion_token`
--

INSERT INTO `sesion_token` (`id`, `ip`, `token`, `user_id`, `logged`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjUxMTYyOCwiZXhwIjoxNjQ2NTE1MjI4LCJuYmYiOjE2NDY1MTE2MjgsImp0aSI6InM5T1VDcTFLNTJaZEtvUjYiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.Rxdu18NJD3h8zSGJ8y3Ng7jJ6DrPG3VBYlCoH8sChss', 1, 1, NULL, NULL),
(2, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjUxMTY1MCwiZXhwIjoxNjQ2NTE1MjUwLCJuYmYiOjE2NDY1MTE2NTAsImp0aSI6IjlRMms0UVd6blBxZ3Vqa2ciLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.wDARVzggu4mVTR_LnDLjUMHyOEaoUNHtNPl0twQilUw', 1, 1, NULL, NULL),
(3, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjUyNjYzMywiZXhwIjoxNjQ2NTMwMjMzLCJuYmYiOjE2NDY1MjY2MzMsImp0aSI6IldYV1dSYlo4TFl3eVY1UEkiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.xxxyh3jEQjfsBQYvPIXTWSCDSSMjeMKqdqAeu_pizZE', 1, 1, NULL, NULL),
(4, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjUzMDU0NCwiZXhwIjoxNjQ2NTM0MTQ0LCJuYmYiOjE2NDY1MzA1NDQsImp0aSI6IjlVM2RkcTZkNWMyRXVsVG8iLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.vr3gwWXD58L-adxzh0f0KzTJeQH-_Pw_XpunlXhDl6U', 1, 1, NULL, NULL),
(5, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjUzMDg5NSwiZXhwIjoxNjQ2NTM0NDk1LCJuYmYiOjE2NDY1MzA4OTUsImp0aSI6IjBsNVZTS1FKRmQ4TGk3M1YiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.Kx619QE-jCm9TnI6HVGh_qOo4j162WYx5rsEjueiFNM', 1, 1, NULL, NULL),
(6, '127.0.0.1', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjUzMzY2NywiZXhwIjoxNjQ2NTM3MjY3LCJuYmYiOjE2NDY1MzM2NjcsImp0aSI6InVmbjNVQlFHbWk2VTRzT3QiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.nFaiHwSQ7OPW8mLQVlKumy6KDw8TCuJjlIjYEsgismk', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `delected` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `delected`, `created_at`, `updated_at`) VALUES
(1, 'Luis', 'luis', 'eyJpdiI6InJhZlVUckZRaW1rQ2VtMkxTZVRvbEE9PSIsInZhbHVlIjoic3NOdlVGNjRFQlkzZmpWRHc0Qm9UQT09IiwibWFjIjoiNDUzNWZkZDc1NDc4MTM4NjIxNzMxY2YyOWUxNzA0ZTMwMzFlY2Y3NDk5NGIzMWVmYWE0YjhiY2Y1MDg1ZDhmNyIsInRhZyI6IiJ9', 1, 0, NULL, NULL),
(2, 'Luis', 'luisss', 'eyJpdiI6IkJlbGwzTXZxazhST3JUZmZQajE5ZlE9PSIsInZhbHVlIjoibTVKYVBVbHVmUFJNdE9XK1BKSTNFZz09IiwibWFjIjoiNzRhODc1ZDk0NTk1YmMxNGYyMDEyYzRiMTFkOTlhMTQ0ZjQ4ZmQ2OWE4Nzg3MDAyMDVkMjUzMzFmYjE1N2Y3ZCIsInRhZyI6IiJ9', 1, 0, NULL, NULL),
(3, 'wedwed', 'dwewedwed', 'eyJpdiI6ImxHQjkrZzQrS2EzdmQrWmFBSVlLeXc9PSIsInZhbHVlIjoiSGx3QUV0b0YzQm4vMFlZZWFuMnN0QT09IiwibWFjIjoiMzE0M2I3NjQ4NjI5NDRkNjI0YThmYTU4YzQ0MmJjMTZhOTAyMDE3NzVmMGQxNzM3OTVmMGRiZmI2MTkxMDQ3YiIsInRhZyI6IiJ9', 1, 0, NULL, NULL),
(4, 'dwewed', 'wedwed', 'eyJpdiI6ImZXZllGZWFoQXJFOFdPTURBeExFNHc9PSIsInZhbHVlIjoibzBWaVhaYmlycUZ0MnY0QVE3MXpTdz09IiwibWFjIjoiNWIwNmY5ZWQ2NWJhOTk1ZTQ0YWYxYWZiZmE4MDdmMTM2NzE1MTFlNjMyYTg1ZGI3NWU4NTU5ODE4M2Y1ZjI5ZCIsInRhZyI6IiJ9', 1, 0, NULL, NULL),
(5, 'wedwed', 'wedwedd', 'eyJpdiI6Iis0U2tVemF1dXZPVjg0bXRCbTFtNVE9PSIsInZhbHVlIjoiNEpNaWtBVmJPZVMyZ0hlVnFvdmt3UT09IiwibWFjIjoiMzRhODJhZTczMWQxNTE5MDkzNzgxNWIxNDEyN2EzMDkwYjM3NDhhZDNlOGQ4ZDcwZGZhYTU5NjY3ZGZmYWJlZCIsInRhZyI6IiJ9', 1, 0, NULL, NULL),
(6, 'dwewed', 'wedwedwed', 'eyJpdiI6IktNekYwZHFCV1M5eFlIUjJnTUdZU1E9PSIsInZhbHVlIjoidXF4QTBHVXdPQXA5MDlzc3EzYjVFdz09IiwibWFjIjoiMzE3MmQxM2M2ZDNhMDNmNGU1MjM5ZTdhZWJmMWVkM2FmYTM0OTFjYTcyMWMyNWVkNmYxZWNkYzJlY2U5N2EzNSIsInRhZyI6IiJ9', 1, 0, NULL, NULL),
(7, 'ee', 'eee', 'eyJpdiI6IlMrbGlmd0txWVZ1N3VXMUswdXYydWc9PSIsInZhbHVlIjoieU1rMlUyK2ZIdm84VTRPT09XTWlHQT09IiwibWFjIjoiYTI4ODZkNzMyZDhlYWU4YjBkNmRkZDI2ZDcwZmE4ZjQ4YzA0N2YyNDk5ZTM2MGYzNmU2NTI0MTViZjk2MzBjOSIsInRhZyI6IiJ9', 1, 0, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acta_reunion`
--
ALTER TABLE `acta_reunion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acta_reunion_responsable_foreign` (`responsable`);

--
-- Indices de la tabla `compromisos`
--
ALTER TABLE `compromisos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compromisos_acta_id_foreign` (`acta_id`);

--
-- Indices de la tabla `lista_asistentes`
--
ALTER TABLE `lista_asistentes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lista_asistentes_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sesion_token`
--
ALTER TABLE `sesion_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sesion_token_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acta_reunion`
--
ALTER TABLE `acta_reunion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `compromisos`
--
ALTER TABLE `compromisos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lista_asistentes`
--
ALTER TABLE `lista_asistentes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sesion_token`
--
ALTER TABLE `sesion_token`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acta_reunion`
--
ALTER TABLE `acta_reunion`
  ADD CONSTRAINT `acta_reunion_responsable_foreign` FOREIGN KEY (`responsable`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `compromisos`
--
ALTER TABLE `compromisos`
  ADD CONSTRAINT `compromisos_acta_id_foreign` FOREIGN KEY (`acta_id`) REFERENCES `acta_reunion` (`id`);

--
-- Filtros para la tabla `lista_asistentes`
--
ALTER TABLE `lista_asistentes`
  ADD CONSTRAINT `lista_asistentes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `sesion_token`
--
ALTER TABLE `sesion_token`
  ADD CONSTRAINT `sesion_token_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
