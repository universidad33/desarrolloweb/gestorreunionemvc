<?php

use App\Http\Controllers\Auth\UserAuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MeetController;
use App\Http\Controllers\RolController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', [UserAuthController::class, 'login']);

Route::group(['middleware' => 'JwtAuth'], function () {
    Route::post('user/register', [UserAuthController::class, 'register']);
    Route::post('auth/me', [UserAuthController::class, 'me']);
    Route::get('user/list', [UserController::class, 'list']);
    Route::get('user/roles', [RolController::class, 'roles']);

    Route::post('meet/addMeet', [MeetController::class, 'createMeet']);
    Route::get('meet/getAllMeet', [MeetController::class, 'getAllMeet']);
    
    Route::post('user/addCompromisos', [MeetController::class, 'addCompromisos']);
});

